;;; lunar-themes.el --- Common components for Lunar -*- lexical-binding: t; eval: (rainbow-mode) -*-

;; Copyright (C) 2020-2021 Lunar Studio

;; Author: Benno <benno@lunar.studio>
;; Maintainer: Benno <benno@lunar.studio>
;; Keywords: themes, theme
;; URL: http://gitlab.com/lunar.studio/lunar-themes
;; Version: 2022.04.08
;; X-Original-Version: 0.1
;; Package-Requires: ((emacs "24.1"))

;; Lunar builds on the work done in the Eziam theme, copyright (C)
;; 2016-2020 Thibault Polge and the Minimal theme, copyright (C) 2014
;; Anler Hp.  Polge also credits other themes for Eziam's base. See
;; https://github.com/thblt/eziam-theme-emacs for more.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; A nearly-monochromic theme brought to you by LUNAR.
;;

;;; Code:

(defmacro lunar-with-color-variables (lunar-colors &rest body)
  "`let' bind all colors defined in LUNAR-COLORS around BODY."
  (declare (indent 0))
  `(let ((class '((class color) (min-colors 89)))
         ,@(mapcar (lambda (c)
                     (list (intern (car c)) (cdr c)))
                   lunar-colors))
     ,@body))

;; Define default LUNAR faces to use
(defface lunar-selector-current '() "LUNAR face for selection highlighting")

(defun lunar-apply-custom-theme (theme-name)
  "Apply the Lunar theme faces under the name THEME-NAME"
  (custom-theme-set-faces
   theme-name
   ;; Sensible Defaults
   ;; ------------------

   ;; Basic Faces
   `(default ((,class (:background ,background :foreground ,foreground))))
   `(bold ((,class (:weight bold :foreground ,comment))))
   `(bold-italic ((,class (:slant italic :weight bold))))
   `(cursor ((,class (:background ,accent :inverse-video t))))
   `(fringe ((,class (:background ,modeline-background :foreground ,comment))))
   `(holiday ((,class (:background ,org-background))))
   `(homoglyph ((,class (:foreground ,accent))))
   `(italic ((,class (:slant italic))))
   `(trailing-whitespace ((,class (:underline (:style wave :foreground ,failure)))))
   `(underline ((,class (:underline t))))
   `(vertical-border ((,class (:foreground ,border))))

   ;; Accent Faces
   `(highlight-numbers-number ((,class (:foreground ,foreground :background ,hl-background))))
   `(highlight ((,class (:foreground ,background :background ,accent))))

   `(lunar-selector-current ((,class (:underline (:style line :color ,accent) :background ,background))))

   `(shadow ((,class (:foreground ,comment))))
   `(error ((,class (:foreground ,failure))))
   `(warning ((,class (:foreground ,attention))))
   `(success ((,class (:foreground ,ok))))

   ;; Syntaxy Faces
   `(font-lock-builtin-face ((,class (:inherit bold))))
   `(font-lock-comment-delimiter-face ((,class (:foreground ,comment-delimiter))))
   `(font-lock-comment-face ((,class (:foreground ,comment))))
   `(font-lock-constant-face ((,class (:inherit bold))))
   `(font-lock-doc-face ((,class (:inherit (font-lock-comment-face)))))
   `(font-lock-doc-string-face ((,class (:inherit italic))))
   `(font-lock-function-name-face ((,class (:inherit bold))))
   `(font-lock-keyword-face ((,class (:inherit bold))))
   `(font-lock-negation-char-face ((,class (:overline t))))
   `(font-lock-preprocessor-face ((,class (:inherit (font-lock-constant-face)))))
   `(font-lock-regexp-grouping-backslash ((,class (:underline (:style line :color ,comment)))))
   `(font-lock-regexp-grouping-construct ((,class (:underline (:style line :color ,comment-delimiter)))))
   `(font-lock-string-face ((,class (:foreground ,foreground :foreground ,string))))
   `(font-lock-type-face ((,class (:foreground ,foreground :slant italic))))
   `(font-lock-variable-name-face ((,class (:foreground ,foreground))))
   `(font-lock-warning-face ((,class (:weight bold :foreground ,attention))))

   ;; Minibuffer
   `(minibuffer-prompt ((,class (:foreground ,minibuffer))))

   ;; Region Selection
   `(region ((,class (:background ,region))))
   `(secondary-selection ((,class (:background ,region))))

   ;; UI
   `(border ((,class (:foreground ,foreground))))
   `(link ((,class (:foreground ,foreground :underline (:style line :color ,accent)))))
   `(link-visited ((,class (:foreground ,foreground
                                        :underline (:style line :color ,foreground)))))

   ;; MODE Specific
   ;; -------------

   ;; Avy
   `(avy-background-face ((,class (:foreground ,foreground))))
   `(avy-goto-char-timer-face ((,class (:foreground ,accent :background ,background))))
   `(avy-lead-face ((,class (:foreground ,accent :background ,background))))
   `(avy-lead-face-0 ((,class (:foreground ,attention :background ,background))))
   `(avy-lead-face-1 ((,class (:foreground ,accent :background ,background))))
   `(avy-lead-face-2 ((,class (:foreground ,attention :background ,background))))

   ;; Ace-window
   `(aw-background-face ((,class (:foreground ,comment-delimiter))))
   `(aw-key-face ((,class (:foreground ,comment))))
   `(aw-leading-char-face ((,class (:foreground ,accent :height 2.0 :inverse-video t))))
   `(aw-minibuffer-leading-char-face ((,class (:foreground ,accent :height 2.0))))
   `(aw-mode-line-face ((,class (:foreground ,comment))))

   ;; Company
   `(company-echo ((,class (:foreground ,foreground))))
   `(company-echo-common ((,class (:inherit company-echo :underline (:style line :color ,accent)))))

   `(company-preview ((,class (:foreground ,foreground :background ,comment-delimiter))))
   `(company-preview-common ((,class (:inherit bold))))
   `(company-preview-search ((,class (:foreground ,string))))
   `(company-scrollbar-bg ((,class (:background ,modeline-background))))
   `(company-scrollbar-fg ((,class (:background ,string))))
   `(company-tooltip ((,class (:background ,modeline-background))))
   `(company-tooltip-annotation ((,class (:foreground ,accent))))
   `(company-tooltip-annotation-selection ((,class (:foreground ,accent))))
   `(company-tooltip-common ((,class (:foreground ,accent))))
   `(company-tooltip-common-selection ((,class (:foreground ,accent))))
   `(company-tooltip-mouse ((,class (:inherit company-tooltip-selection))))
   `(company-tooltip-search ((,class (:inherit company-tooltip-selection))))
   `(company-tooltip-search-selection ((,class (:inherit company-tooltip-selection))))
   `(company-tooltip-selection ((,class (:inherit company-tooltip :background ,comment-delimiter))))

   ;; Completions
   `(completions-common-part ((,class (:foreground ,failure))))

   ;; Cua
   `(cua-global-mark ((,class (:inherit highlight))))
   `(cua-rectangle ((,class (:inherit region :slant oblique))))
   `(cua-rectangle-noselect ((,class (:inherit cua-rectangle :strike-through ,accent))))

   ;; Custom
   `(custom-button-pressed-unraised ((,class (:inherit custom-button-unraised :foreground ,attention))))
   `(custom-button-changed ((,class (:background ,attention :foreground ,background))))
   `(custom-changed ((,class (:foreground ,foreground :slant italic))))
   `(custom-comment ((,class (:foreground ,comment))))
   `(custom-comment-tag ((,class (:foreground ,comment))))
   `(custom-group-tag ((,class (:inherit variable-pitch :height 1.2 :weight bold))))
   `(custom-group-tag-1 ((,class (:inherit custom-group-tag :foreground ,string))))
   `(custom-invalid ((,class (:inherit default :strike-through ,failure))))
   `(custom-modified ((,class (:foreground ,foreground :background ,org-background))))
   `(custom-rogue ((,class (:foreground ,failure :background ,foreground))))
   `(custom-set ((,class (:foreground ,attention))))
   `(custom-state ((,class (:inherit bold))))
   `(custom-themed ((,class (:foreground ,foreground :underline (:style line :color ,ok)))))
   `(custom-group-tag-1 ((,class (:inherit custom-group-tag :foreground ,comment))))
   `(custom-variable-tag ((,class (:inherit bold))))

   ;; Dired
   `(dired-directory ((,class (:inherit font-lock-function-name-face :weight normal))))
   `(dired-special ((,class (:inherit font-lock-variable-name-face :foreground ,accent))))

   ;; Doom-modeline
   `(doom-modeline-buffer-path ((,class (:inherit mode-line-buffer-id))))
   `(doom-modeline-buffer-file ((,class (:inherit mode-line-buffer-id))))
   `(doom-modeline-buffer-modified ((,class (:inherit error :background nil))))

   ;; Epa
   `(epa-string ((,class (:foreground ,string))))

   ;; ERT
   `(ert-test-result-expected ((,class (:background ,ok))))
   `(ert-test-result-unexpected ((,class (:background ,failure))))

   ;; EShell
   `(eshell-prompt ((,class (:foreground ,string))))

   ;; Flycheck
   `(flycheck-error ((,class (:underline (:style wave :color ,failure)))))
   `(flycheck-info ((,class (:underline (:style wave :color ,foreground)))))
   `(flycheck-warning ((,class (:underline (:style wave :color ,attention)))))

   ;; Flymake
   `(flymake-error ((,class (:underline (:style wave :color ,failure)))))
   `(flymake-note ((,class (:underline (:style wave :color ,foreground)))))
   `(flymake-warning ((,class (:underline (:style wave :color ,attention)))))

   ;; Flyspell
   `(flyspell-duplicate ((,class (:underline (:style wave :color ,attention)))))
   `(flyspell-incorrect ((,class (:underline (:style wave :color ,failure)))))

   ;; Helm
   `(helm-candidate-number ((,class (:foreground ,foreground :background ,region))))
   `(helm-header-line-left-margin ((,class (:inherit default))))
   `(helm-match ((,class (:foreground ,accent))))
   `(helm-prefarg ((,class (:slant italic))))
   `(helm-selection ((,class (:inherit default :underline (:style line :color ,accent)))))
   `(helm-separator ((,class (:foreground ,accent))))
   `(helm-source-header
     ((,class (:font-family "Sans Serif"
                            :height 1.3 :weight bold
                            :foreground ,modeline-foreground
                            :background ,modeline-background))))
   `(helm-visible-mark ((,class (:foreground ,accent))))

   ;; Help key binding
   `(help-key-binding ((,class (:background ,hl-face-background :box (:line-width (1 . -1) :color ,string) :foreground ,foreground))))

   ;; HL Line
   `(hl-line ((,class (:background ,hl-background))))
   `(hl-line-face ((,class (:background ,hl-face-background))))
   `(hl-todo ((,class (:weight bold :foreground ,ok))))

   ;; highlight-stages-mode (copied from Minimal -- thanks!)
   `(highlight-stages-negative-level-face ((,class (:foreground ,failure))))
   `(highlight-stages-level-1-face ((,class (:background ,org-background))))
   `(highlight-stages-level-2-face ((,class (:background ,region))))
   `(highlight-stages-level-3-face ((,class (:background ,region))))
   `(highlight-stages-higher-level-face ((,class (:background ,region))))

   ;; Hydra
   `(hydra-face-amaranth ((,class (:inherit bold))))
   `(hydra-face-blue ((,class (:inherit bold))))
   `(hydra-face-pink ((,class (:inherit bold))))
   `(hydra-face-red ((,class (:inherit bold-italic))))
   `(hydra-face-teal ((,class (:inherit bold))))

   ;; IBuffer
   `(ibuffer-locked-buffer ((,class (:foreground ,accent))))

   ;; IDO
   `(ido-indicator ((,class (:foreground ,accent :background ,foreground))))
   `(ido-subdir ((,class (:inherit bold))))
   `(ido-only-match ((,class (:inherit bold))))

   ;; Info
   `(info-menu-star ((,class (:foreground ,failure))))

   ;; Isearch
   `(isearch ((,class (:foreground ,foreground :background ,region :weight normal))))
   `(isearch-fail ((,class (:foreground ,failure :bold t))))
   `(lazy-highlight ((,class (:foreground ,foreground :background ,region))))

   ;; Ivy
   `(ivy-confirm-face ((,class (:foreground ,ok))))
   `(ivy-current-match ((,class (:inherit lunar-selector-current))))
   `(ivy-cursor ((,class (:inherit cursor))))
   `(ivy-highlight-face ((,class (:background ,border :box (:color ,background :line-width -4)))))
   `(ivy-match-required-face ((,class (:inherit minibuffer-prompt :color ,failure))))
   `(ivy-minibuffer-match-face-1 ((,class (:background ,org-background))))
   `(ivy-minibuffer-match-face-2 ((,class (:background ,modeline-background))))
   `(ivy-minibuffer-match-face-3 ((,class (:background ,comment-delimiter))))
   `(ivy-minibuffer-match-face-4 ((,class (:background ,comment))))
   `(ivy-remote ((,class (:foreground ,string))))

   ;; JS2
   `(js2-error ((,class (:foreground ,failure))))
   `(js2-external-variable ((,class (:inherit base-faces :weight bold))))
   `(js2-function-param ((,class (:inherit base-faces))))
   `(js2-instance-member ((,class (:inherit base-faces))))
   `(js2-jsdoc-html-tag-delimiter ((,class (:inherit base-faces))))
   `(js2-jsdoc-html-tag-name ((,class (:inherit base-faces))))
   `(js2-jsdoc-tag ((,class (:inherit base-faces))))
   `(js2-jsdoc-type ((,class (:inherit base-faces :weight bold))))
   `(js2-jsdoc-value ((,class (:inherit base-faces))))
   `(js2-magic-paren ((,class (:underline t))))
   `(js2-private-function-call ((,class (:inherit base-faces))))
   `(js2-private-member ((,class (:inherit base-faces))))

   ;; Linum Relative
   `(linum-relative-current-face ((,class (:foreground ,accent :background ,foreground))))

   ;; Magit
   ;; In general, these are ok, but the highlight needs some work.
   `(magit-section-highlight ((,class (:foreground ,foreground :background ,hl-face-background))))

   ;; Match
   `(match ((,class ((:foreground ,foreground :background ,org-background :underline (:style line :color ,accent))))))

   ;; Multiple Cursors
   `(mc/cursor-bar-face ((,class (:inherit cursor))))
   `(mc/cursor-face ((,class (:inherit cursor))))

   ;; Mode-line
   `(mode-line
     ((,class (:foreground ,modeline-foreground :background ,modeline-background
                           :box (:color ,comment)
                           :height 0.8))))
   `(mode-line-emphasis ((,class  (:inherit mode-line :slant italic))))
   `(mode-line-highlight ((,class (:inherit mode-line :slant italic))))
   `(mode-line-buffer-id ((,class (:inherit mode-line :slant italic))))
   `(mode-line-inactive
     ((,class (:foreground ,modeline-foreground-inactive
                           :background ,modeline-background-inactive
                           :box (:color ,comment)
                           :height 0.8))))

   ;; Orderless
   `(orderless-match-face-0 ((,class (:foreground ,accent))))
   `(orderless-match-face-1 ((,class (:foreground ,comment))))
   `(orderless-match-face-2 ((,class (:foreground ,comment-delimiter))))
   `(orderless-match-face-3 ((,class (:foreground ,region))))

   ;; Org-mode
   `(org-agenda-date ((,class (:foreground ,minibuffer))))
   `(org-agenda-done ((,class (:foreground ,ok))))
   `(org-agenda-structure ((,class (:foreground ,accent))))

   `(org-block ((,class (:background ,org-background))))
   `(org-block-background ((,class (:background ,org-background :foreground ,foreground))))
   `(org-block-begin-line
     ((,class (:background ,org-background :foreground ,comment-delimiter))))
   `(org-block-end-line
     ((,class (:background ,org-background :foreground ,comment-delimiter))))

   `(org-code ((,class (:background ,org-background))))

   `(org-column ((,class (:foreground ,foreground :background ,region :weight medium :slant normal :underline nil :strike-through nil :height 0.8))))
   `(org-column-title ((,class (:foreground ,foreground :background ,region :weight bold :underline (:style line :foreground ,foreground) :height 0.9))))

   `(org-date ((,class (:background ,org-background :underline t))))
   `(org-date-selected ((,class (:foreground ,accent :inverse-video t))))

   `(org-drawer ((,class (:foreground ,comment))))
   `(org-document-title ((,class (:foreground ,foreground))))
   `(org-done ((,class (:foreground ,comment :strike-through ,ok))))
   `(org-ellipsis ((,class (:foreground ,accent))))
   `(org-footnote ((,class (:slant italic))))
   `(org-formula ((,class (:inherit font-lock-function-name-face))))
   `(org-headline-done ((,class (:strike-through ,ok))))
   `(org-latex ((,class (:foreground ,comment))))
   `(org-latex-and-related ((,class (:inherit org-latex))))

   `(org-level-1 ((,class (:foreground ,foreground :height 1.5 :overline ,comment-delimiter))))
   `(org-level-2 ((,class (:foreground ,foreground :height 1.4))))
   `(org-level-3 ((,class (:foreground ,comment :height 1.3))))
   `(org-level-4 ((,class (:foreground ,comment :height 1.2))))
   `(org-level-5 ((,class (:foreground ,comment :height 1.1))))
   `(org-level-6 ((,class (:foreground ,comment :slant italic))))
   `(org-level-7 ((,class (:foreground ,comment :slant italic))))
   `(org-level-8 ((,class (:foreground ,comment :slant italic))))

   `(org-scheduled ((,class (:slant italic))))
   `(org-scheduled-previously ((,class (:inherit org-scheduled :foreground ,attention))))
   `(org-scheduled-today ((,class (:inherit org-scheduled :weight bold))))
   `(org-sexp-date ((,class (:foreground ,comment))))

   `(org-link ((,class (:inherit link))))
   `(org-mode-line-clock-overrun ((,class (:inherit mode-line :background ,failure))))
   `(org-table ((,class (:background ,org-background))))
   `(org-tag ((,class (:background ,org-background :foreground ,foreground))))
   `(org-todo ((,class (:foreground ,foreground :weight bold :overline ,ok))))
   `(org-upcoming-deadline ((,class (:foreground ,attention))))
   `(org-warning ((,class (:foreground ,attention :weight bold))))

   ;; Org Roam
   `(org-roam-link ((,class (:inherit link))))
   `(org-roam-link-current ((,class (:inherit link))))
   `(org-roam-link-invalid ((,class (:inherit link :foreground ,accent))))

   ;; Outine
   ;; looks the same as `org-mode`
   `(outline-1 ((,class (:inherit org-level-1))))
   `(outline-2 ((,class (:inherit org-level-2))))
   `(outline-3 ((,class (:inherit org-level-3))))
   `(outline-4 ((,class (:inherit org-level-4))))
   `(outline-5 ((,class (:inherit org-level-5))))
   `(outline-6 ((,class (:inherit org-level-6))))
   `(outline-7 ((,class (:inherit org-level-7))))
   `(outline-8 ((,class (:inherit org-level-8))))

   ;; Tool Tips
   `(tool-tip ((,class (:inherit variable-pitch :foreground ,comment :background ,org-background))))

   ;; Selectrum
   `(selectrum-current-candidate ((,class (:inherit lunar-selector-current))))

   ;; Shell Mode
   `(sh-heredoc ((,class (:foreground ,attention :weight bold))))
   `(sh-quoted-exec ((,class (:foreground ,ok :weight bold))))

   ;; Show Paren
   `(show-paren-match ((,class (:background ,region))))
   `(show-paren-mismatch ((,class (:foreground ,failure :weight bold))))

   ;; Undo Tree
   `(undo-tree-visualizer-active-branch-face ((,class (:inherit bold))))
   `(undo-tree-visualizer-current-face ((,class (:foreground ,accent))))
   `(undo-tree-visualizer-default-face ((,class (:foreground ,foreground))))
   `(undo-tree-visualizer-register-face ((,class (:box (:color ,comment)))))
   `(undo-tree-visualizer-unmodified-face ((,class (:inherit default))))

   ;; Vertico
   `(vertico-current ((,class (:inherit lunar-selector-current))))

   ;; Widget faces
   `(widget-field ((,class (:foreground ,background :background ,foreground))))
   `(widget-single-line-field ((,class (:inherit widget-field))))))

;; Local Variables:
;; hl-sexp-mode: nil
;; END:

(provide 'lunar-themes)
;;; lunar-themes.el ends here
