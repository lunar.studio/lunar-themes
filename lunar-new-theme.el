;;; lunar-new-theme.el --- A minimal theme -*- eval: (rainbow-mode) -*-

;; Copyright (C) 2020-2021 Lunar Studio

;; Author: Benno <benno@lunar.studio>
;; Maintainer: Benno <benno@lunar.studio>
;; Keywords: themes, theme, faces
;; URL: http://gitlab.com/lunar.studio/lunar-themes
;; Version: 2022.04.08
;; X-Original-Version: 0.1
;; Package-Requires: ((emacs "24.1"))

;; Lunar builds on the work done in the Eziam theme, copyright (C)
;; 2016-2020 Thibault Polge and the Minimal theme, copyright (C) 2014
;; Anler Hp.  Polge also credits other themes for Eziam's base. See
;; https://github.com/thblt/eziam-theme-emacs for more.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; A nearly-monochromic dark theme brought to you by LUNAR.
;;

;;; Code:
(require 'lunar-themes)
(deftheme lunar-new "monochromaticish dark theme with blue accent")

(lunar-with-color-variables
  (("accent"                       . "#1D7EC7")
   ("foreground"                   . "grey90")
   ("background"                   . "grey15")
   ("cursor"                       . "grey70")
   ("border"                       . "grey30")
   ("minibuffer"                   . "grey70")
   ("region"                       . "black")
   ("comment-delimiter"            . "grey45")
   ("comment"                      . "grey60")
   ("constant"                     . "grey90")
   ("string"                       . "grey40")
   ("modeline-foreground"          . "grey90")
   ("modeline-background"          . "grey25")
   ("modeline-foreground-inactive" . "grey60")
   ("modeline-background-inactive" . "grey15")
   ("hl-background"                . "black")
   ("hl-face-background"           . "black")
   ("org-background"               . "grey20")
   ("failure"                      . "#F03E3E")
   ("attention"                    . "#C76E1D")
   ("ok"                           . "#19AD19"))
  (lunar-apply-custom-theme 'lunar-new))

;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'lunar-new)

;; Local Variables:
;; hl-sexp-mode: nil
;; END:

;;; lunar-new-theme.el ends here
