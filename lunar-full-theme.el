;;; lunar-full-theme.el --- A minimal theme -*- eval: (rainbow-mode) -*-

;; Copyright (C) 2020-2021 Lunar Studio

;; Author: Benno <benno@lunar.studio>
;; Maintainer: Benno <benno@lunar.studio>
;; Keywords: themes, theme, faces
;; URL: http://gitlab.com/lunar.studio/lunar-themes
;; Version: 2022.04.08
;; X-Original-Version: 0.1
;; Package-Requires: ((emacs "24.1"))

;; Lunar builds on the work done in the Eziam theme, copyright (C)
;; 2016-2020 Thibault Polge and the Minimal theme, copyright (C) 2014
;; Anler Hp.  Polge also credits other themes for Eziam's base. See
;; https://github.com/thblt/eziam-theme-emacs for more.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; A nearly-monochromic light theme brought to you by LUNAR.
;;

;;; Code:
(require 'lunar-themes)
(deftheme lunar-full "monochromaticish light theme")

(lunar-with-color-variables
  (("accent"                       . "#C7AA1D")
   ("foreground"                   . "grey20")
   ("background"                   . "white")
   ("cursor"                       . "black")
   ("border"                       . "grey90")
   ("minibuffer"                   . "black")
   ("region"                       . "grey90")
   ("comment-delimiter"            . "grey85")
   ("comment"                      . "grey70")
   ("constant"                     . "grey20")
   ("string"                       . "grey40")
   ("modeline-foreground"          . "grey20")
   ("modeline-background"          . "grey95")
   ("modeline-foreground-inactive" . "grey70")
   ("modeline-background-inactive" . "white")
   ("hl-background"                . "grey90")
   ("hl-face-background"           . "grey95")
   ("org-background"               . "grey95")
   ("failure"                      . "#C71D1D")
   ("attention"                    . "#C76E1D")
   ("ok"                           . "#1DC71D"))
  (lunar-apply-custom-theme 'lunar-full))


;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'lunar-full)

;; Local Variables:
;; hl-sexp-mode: nil
;; END:

;;; lunar-full-theme.el ends here
