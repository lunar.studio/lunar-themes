#+options: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+options: author:t broken-links:nil c:nil creator:nil
#+options: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:nil
#+options: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+options: timestamp:t title:t toc:t todo:t |:t
#+title: Lunar Themes
#+author: Benno
#+email: benno@lunar.studio
#+creator: Emacs 26.3 (Org mode 9.3.7)

These two themes are inspired by (and based partly on) the fantastic
work by Anler's [[https://github.com/anler/minimal-theme][minimal-theme]] and Thibault Polge's [[https://github.com/thblt/eziam-theme-emacs][eziam-theme]]. These
two themes start with the idea of an accent color and work that into a
largely black, white, and grey theme.

* Two styles

  The themes come in two styles. The dark one, /New/, and a light one,
  /Full/.

** Colors

   There are four colors that make an apperance in the theming. They
   are listed in the following table.

   | Color Type | New     | Full    |
   |------------+---------+---------|
   | Accent     | #1D7EC7 | #C7AA1D |
   | Failure    | #F03E3E | #C71D1D |
   | Attention  | #C76E1D | #C76E1D |
   | Ok         | #19AD19 | #1DC71D |

** Screenshots

   #+attr_html: :width 100px
   #+CAPTION: Full Theme
   [[./full.png]]

   #+attr_html: :width 100px
   #+CAPTION: New Theme
   [[./new.png]]
